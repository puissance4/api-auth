'use strict';
module.exports = (sequelize, DataTypes) => {
  const Platform = sequelize.define('Platform', {
    platformName: DataTypes.STRING
  }, {});
  /*Platform.associate = function(models) {
    models.Platform.hasMany(models.Account)
  };*/
  return Platform;
};