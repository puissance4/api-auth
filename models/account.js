'use strict';
module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define('Account', {
    idUsers: DataTypes.INTEGER,
    login: DataTypes.STRING,
    password: DataTypes.STRING,
    idPlatform: DataTypes.INTEGER
  }, {
    classMethods: {
      associate: function(models) {
        models.Account.belongsTo(models.User, {
          foreignKey: {
            allowNull: false
          }
        })
    },
  }
  });
return Account;
};
 
