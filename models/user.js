'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: DataTypes.STRING,
    username: DataTypes.STRING,
    password: DataTypes.STRING,
    bio: DataTypes.STRING,
    isAdmin: DataTypes.BOOLEAN,
    favorite: DataTypes.STRING
  }, {});
  User.associate = function(models) {
    models.User.hasMany(models.Account)
  };
  return User;
};