//Imports
var jwt = require('jsonwebtoken');

const JWT_SIGN_SECRET = '4aM7zpv0WTFijGGcZr9IJHb3SQETG1YM1134t3OMN7RkSayCDSFlbeaS6Ic4Yov31tfn3EIDjPE1XsIk'

module.exports = {
    generateTokenForUser: function(userData) {
        return jwt.sign({
            userId: userData.id,
            isAdmin: userData.isAdmin
        },
        //choisir le temps de peremption du token
        JWT_SIGN_SECRET,
        {
            expiresIn: '1h'
        })
    },

    //Attention à l'espace après Bearer
    parseAuthorization: function(authorization) {
        return (authorization != null) ? authorization.replace('Bearer ','') : null;
    },

    //récupérer Id utilisateur
    getUserId: function(authorization){

        var userId = -1;
        var token = module.exports.parseAuthorization(authorization);

        if (token != null) {
            try {
                var jwtToken = jwt.verify(token, JWT_SIGN_SECRET);

                if(jwtToken != null)
                    userId = jwtToken.userId;
                 } 
            catch(err) {}
            
        return userId;
     }
    }
}