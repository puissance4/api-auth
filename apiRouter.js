//Imports
var express = require('express');
var usersCtrl = require('./routes/usersCtrl');
var accountsCtrl = require('./routes/accountsCtrl');
var gamesCtrl = require('./routes/gamesCtrl');

// Routeur 
exports.router = (function() {
    var apiRouter = express.Router();

    //Users => routes
    //inscription
    apiRouter.route('/users/register/').post(usersCtrl.register);
    //authentification
    apiRouter.route('/users/login/').post(usersCtrl.login);
    //avoir le profil utilisateur
    apiRouter.route('/users/profil/').get(usersCtrl.getUserProfile);
    //update le profil utilisateur
    apiRouter.route('/users/profil/').put(usersCtrl.updateUserProfile);

    //Accounts => routes
    //Enregistrer un compte Steam
    apiRouter.route('/accounts/new/').post(accountsCtrl.registerAccount);

    //News => routes
    //Recuperer les dernières news depuis IGDB
    apiRouter.route('/games/lastGames/').get(gamesCtrl.getGames);

return apiRouter; 

//Attention les parenthèses sont importantes => instantiation du router
})();
