// Imports
var bcrypt = require('bcrypt');
var jwt = require('jsonwebtoken');
var models = require('../models');
var jwtUtils = require('../utils/jwt.utils');
var asyncLib = require('async');


//Routes
module.exports = {
    //Fonction d'enregistrement POST
    registerAccount: function(request, response) {
         //Params
         var headerAuth = request.headers['authorization'];
         var userId = jwtUtils.getUserId(headerAuth);

        var platform = request.body.platform;
        var login = request.body.login;
 
         if(userId<0)
             return response.status(400).json({'error':'Token erroné '+userId});
 
         models.User.findOne({
             attributes: ['ID', 'email', 'username'],
             where: {id: userId}
         })
         .then(function(user) {
             if(user) {
                 response.status(201).json(user);
                 var newAccount = models.Account.create({
                    idUsers:userId,
                    idPlatform: platform,
                    login: login
                });
                return response.status(200).json(newAccount);
             }
             else {
                 response.status(404).json({'error':'Utilisateur non trouvé'})
             }
         })
         .catch(function(err) {
             response.status(500).json({'error': 'impossible de trouver cet utilisateur '+err})
         });
     },
    }
